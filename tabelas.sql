

CREATE TABLE cliente (cd_cliente        NUMBER(4)
                     ,nm_razao_social   VARCHAR2(100)
                     ,nm_fantasia       VARCHAR2(50)
                     ,dt_criacao        DATE
                     ,ic_ativo          CHAR(1)
                     ,CONSTRAINT nn_cliente_razao_social CHECK (nm_razao_social IS NOT NULL)
                     ,CONSTRAINT pk_cliente PRIMARY KEY(cd_cliente)
                     ,CONSTRAINT ck_cliente_ativo CHECK (ic_ativo IN ('S', 'N'))
                     ,CONSTRAINT nn_cliente_ativo CHECK (ic_ativo IS NOT NULL)
                     ); 
                     
Create TABLE telefone (cd_telefone     NUMBER(2)
                      ,cd_cliente      NUMBER(4)
                      ,nr_ddi          NUMBER(3)
                      ,nr_ddd          NUMBER(3)
                      ,nr_telefone     NUMBER(3)
                      ,CONSTRAINT pk_cd_telefone PRIMARY KEY(cd_cliente,cd_telefone)
                      ,CONSTRAINT fk_cd_cliente_telefone FOREIGN KEY 
                           (cd_cliente) REFERENCES cliente(cd_cliente)
                      ,CONSTRAINT nn_nr_ddi CHECK(nr_ddi IS NOT NULL)
                      ,CONSTRAINT nn_nr_ddd CHECK(nr_ddd IS NOT NULL)
                      ,CONSTRAINT nn_nr_telefone CHECK(nr_telefone IS NOT NULL)
                      );
                     
CREATE TABLE produto  (cd_produto          NUMBER(4) 
                      ,ds_produto          VARCHAR(30)
                      ,tp_produto          CHAR(1)
                      ,nr_dias_vencimento  NUMBER(3)
                      ,CONSTRAINT nn_ds_produto CHECK 
                             (ds_produto IS NOT NULL)                
                      ,CONSTRAINT pk_produto PRIMARY KEY(cd_produto)
                      );
                      
CREATE TABLE endereco (cd_endereco    NUMBER(4)
                      ,cd_cliente     NUMBER(4)
                      ,nm_logradouro  VARCHAR(60)
                      ,nr_endereco    NUMBER(6)
                      ,tp_endereco    NUMBER(2)
                      ,ds_complemento VARCHAR(20)
                      ,CONSTRAINT fk_cd_cliente_cliente_endereco FOREIGN KEY
                             (cd_cliente) REFERENCES cliente(cd_cliente)
                      ,CONSTRAINT pk_endereco PRIMARY KEY(cd_cliente, cd_endereco)
                      ,CONSTRAINT nm_endereco_logradouro CHECK (nm_logradouro IS NOT NULL)
                      ,CONSTRAINT nn_nr_endereco CHECK (nr_endereco IS NOT NULL)
                      
                      );
                

                        
CREATE TABLE cliente_x_produto (ic_ativo   CHAR
                               ,cd_cliente NUMBER(4)
                               ,cd_produto NUMBER(4)
                               ,CONSTRAINT pk_cliente_x_produto PRIMARY KEY (cd_cliente, cd_produto)
                               ,CONSTRAINT fk_cd_cliente_produto 
                                  FOREIGN KEY(cd_cliente) REFERENCES cliente(cd_cliente)
                               ,CONSTRAINT fk_cd_produto_cliente_produto
                                  FOREIGN KEY(cd_produto) REFERENCES produto(cd_produto)
                               ,CONSTRAINT ck_ic_ativo CHECK (ic_ativo IN ('S','N'))
                               ,CONSTRAINT nn_ic_ativo CHECK (ic_ativo IS NOT NULL)
                               );
                               
CREATE TABLE transacao (cd_transacao    NUMBER(4)
                       ,cd_cliente      NUMBER(4)
                       ,cd_produto      NUMBER(4)
                       ,vl_transacao    NUMBER(9,2)
                       ,dt_transacao    DATE
                       ,CONSTRAINT pk_transacao PRIMARY KEY(cd_cliente, cd_produto, cd_transacao)
                       ,CONSTRAINT fk_cd_cliente_transacao FOREIGN KEY
                              (cd_cliente, cd_produto) REFERENCES cliente_x_produto(cd_cliente, cd_produto)
                       ,CONSTRAINT ck_vl_transacao CHECK (vl_transacao IS NOT NULL)
                       ,CONSTRAINT ck_dt_transacao CHECK (dt_transacao IS NOT NULL)
                        );
                               
CREATE TABLE comissao_cliente (vl_comissao   NUMBER(5,2)
                              ,cd_cliente    NUMBER(4)
                              ,CONSTRAINT pk_comissao_cliente PRIMARY KEY (cd_cliente)   
                              ,CONSTRAINT fk_cd_cliente_comissao_cliente
                                 FOREIGN KEY(cd_cliente) REFERENCES cliente(cd_cliente)
                              ,CONSTRAINT nn_vl_comissao CHECK (vl_comissao IS NOT NULL)
                              );
                              
           
                      
